// Copyright (C) 2021 rdwn
// 
// This file is part of Clocker.
// 
// Clocker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Clocker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Clocker.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __COMMON_INC_H
#define __COMMON_INC_H

#include "../types/inc/basics.h"
#include "./defs.h"

// #ifdef USE_GEN_TYPE
// #include "../types/inc/gen_type.h"
// #endif // USE_GEN_TYPE

// #ifdef USE_OPTION
#include "../types/inc/option.h"
// #endif // USE_OPTION

// #ifdef USE_RESULT
#include "../types/inc/result.h"
// #endif // USE_RESULT

// #ifdef USE_ASSERT
#include "../prims/inc/assert.h"
// #endif // USE_RESULT

// #ifdef USE_DEBUG
#include "../prims/inc/debug.h"
// #endif // USE_RESULT

#endif // __COMMON_INC_H